﻿using System;
using System.Collections.Generic;

namespace Module3_2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            throw new NotImplementedException();
        }

        public int[] GetFibonacciSequence(int n)
        {
            throw new NotImplementedException();
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            throw new NotImplementedException();
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            throw new NotImplementedException();
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            throw new NotImplementedException();
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            throw new NotImplementedException();
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            throw new NotImplementedException();
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            throw new NotImplementedException();
        }
    }
}
